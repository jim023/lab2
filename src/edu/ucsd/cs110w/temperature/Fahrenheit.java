package edu.ucsd.cs110w.temperature;

public class Fahrenheit extends Temperature
{
   public Fahrenheit(float t)
   {
	   super(t);
   }
   public String toString()
   {
	   // TODO: Complete this method
	   return ""+this.getValue()+" F";
   }
@Override
public Temperature toCelsius() {
	// TODO Auto-generated method stub
	Celsius tmp1 = new Celsius(5*((this.getValue()-32))/9);
	return tmp1;
}
@Override
public Temperature toFahrenheit() {
	// TODO Auto-generated method stub
	Fahrenheit tmp = new Fahrenheit(this.getValue());
	return tmp;
}
@Override
public Temperature toKelvin() {
	// TODO Auto-generated method stub
	Kelvin tmp2 = new Kelvin(5*((this.getValue()-32))/9+273);
	return tmp2;
}
}
