package edu.ucsd.cs110w.temperature;

public class Celsius extends Temperature{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		// TODO: Complete this method
		return ""+this.getValue()+" C";
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Celsius tmp = new Celsius(this.getValue());
        return tmp;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
	    Fahrenheit tmp1 = new Fahrenheit(9*(this.getValue())/5+32);
	    return tmp1;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Kelvin tmp2 = new Kelvin(this.getValue()+273);
		return tmp2;
	}
}
