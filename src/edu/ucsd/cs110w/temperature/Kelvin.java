package edu.ucsd.cs110w.temperature;

/**
 * TODO (jim023): write class javadoc
 * @author jim023
 *
 */
public class Kelvin extends Temperature{
	public Kelvin(float t)
	{
	super(t);
	}
	public String toString()
	{
	// TODO: Complete this method
	return ""+this.getValue()+" K";
	}
	@Override
	public Temperature toCelsius() {
	// TODO: Complete this method
	    Celsius tmp = new Celsius(this.getValue()-273); 
		return tmp;
	}
	@Override
	public Temperature toFahrenheit() {
	// TODO: Complete this method
	    Fahrenheit tmp1 = new Fahrenheit(9*(this.getValue()-273)/5 +32);
		return tmp1;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Kelvin tmp2 = new Kelvin(this.getValue());
		return tmp2;
	}
}
